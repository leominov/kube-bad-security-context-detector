package main

import "strings"

type ErrorSet []error

func (e ErrorSet) StringSlice() []string {
	var result []string
	for _, err := range e {
		result = append(result, err.Error())
	}
	return result
}

func (e ErrorSet) String() string {
	return strings.Join(e.StringSlice(), ", ")
}

func (e ErrorSet) Len() int {
	return len(e)
}
