package main

import (
	"context"
	"fmt"
	"regexp"
	"time"

	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
)

type Detector struct {
	deploymentLabelSelector string
	k8sClient               kubernetes.Interface
	namespaceFilter         string
}

func (d *Detector) Run() (*Report, error) {
	ctx := context.Background()
	report := &Report{
		ScanDate: time.Now(),
	}

	namespaceList, err := d.k8sClient.CoreV1().Namespaces().List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	namespaceFilterRE := regexp.MustCompile(d.namespaceFilter)
	for _, namespace := range namespaceList.Items {
		if !namespaceFilterRE.MatchString(namespace.Name) {
			continue
		}
		reportItems, err := d.processNamespace(ctx, namespace)
		if err != nil {
			return nil, err
		}
		if len(reportItems) == 0 {
			continue
		}
		report.Items = append(report.Items, reportItems...)
	}

	report.TotalItems = len(report.Items)
	return report, nil
}

func (d *Detector) processNamespace(ctx context.Context, namespace v1.Namespace) ([]*ReportItem, error) {
	deployList, err := d.k8sClient.AppsV1().Deployments(namespace.Name).List(ctx, metav1.ListOptions{
		LabelSelector: d.deploymentLabelSelector,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to list deployments in %s namespace", namespace.Name)
	}

	var reportItems []*ReportItem
	for _, deploy := range deployList.Items {
		spec := deploy.Spec.Template.Spec
		for _, container := range spec.Containers {
			errorSet := ValidateContainer(container)
			if errorSet == nil {
				continue
			}
			item := &ReportItem{
				Container:  container.Name,
				Deployment: deploy.Name,
				Errors:     errorSet.StringSlice(),
				ErrorsLine: errorSet.String(),
				Namespace:  namespace.Name,
			}
			reportItems = append(reportItems, item)
		}
	}

	return reportItems, nil
}
