package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/core/v1"
)

func TestValidateSecurityContext(t *testing.T) {
	errorSet := ValidateSecurityContext(nil)
	assert.Equal(t, 1, errorSet.Len())

	securityContext := &v1.SecurityContext{}
	assert.Greater(t, ValidateSecurityContext(securityContext).Len(), 1)
}

func TestValidateImage(t *testing.T) {
	tests := map[string]bool{
		"foobar":             false,
		"foobar:ABCD":        true,
		"foobar:latest":      false,
		"foobar@sha256:ABCD": true,
	}
	for image, valid := range tests {
		assert.Equal(t, valid, ValidateImage(image) == nil)
	}
}
