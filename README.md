# kube-bad-security-context-detector

Поиск шаблонов подов с некорректной конфигурацией Security Context контейнера. Критерии описаны в файле `kubernetes.go`.

## Запуск

```shell
Usage of ./kube-bad-security-context-detector:
  -deployment-label-selector string
    	Label selector for deployments (default "name!=tiller")
  -log-level string
    	Level of logging (default "info")
  -namespace-filter string
    	Filter for namespaces (default "app")
  -report-format string
    	Report format (csv, json, yaml) (default "csv")
```

## Пример отчета

```json
{
  "items": [
    {
      "container": "backend",
      "deployment": "beauty-partner-web-master",
      "errors": [
        "empty security context"
      ],
      "namespace": "app-beauty-partner-web",
    }
  ],
  "scan_date": "2021-07-14T09:05:14.504896+05:00",
  "total_items": 1
}
```

## Ссылки

* https://kubesec.io
