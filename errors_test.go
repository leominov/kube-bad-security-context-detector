package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestErrorSet_StringSlice(t *testing.T) {
	errorSet := ErrorSet{
		errors.New("aaa"),
		errors.New("zzz"),
	}
	assert.Equal(t, []string{"aaa", "zzz"}, errorSet.StringSlice())
}

func TestErrorSet_Len(t *testing.T) {
	errorSet := ErrorSet{
		errors.New("aaa"),
		errors.New("zzz"),
	}
	assert.Equal(t, 2, errorSet.Len())
}

func TestErrorSet_String(t *testing.T) {
	errorSet := ErrorSet{
		errors.New("aaa"),
		errors.New("zzz"),
	}
	assert.Equal(t, "aaa, zzz", errorSet.String())
}
