package main

import (
	"errors"
	"strings"

	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

func NewClientSet() (kubernetes.Interface, error) {
	loadingRules := clientcmd.NewDefaultClientConfigLoadingRules()
	kubeConfig := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(loadingRules, &clientcmd.ConfigOverrides{})
	config, err := kubeConfig.ClientConfig()
	if err != nil {
		return nil, err
	}
	return kubernetes.NewForConfig(config)
}

func ValidateContainer(container v1.Container) (errorSet ErrorSet) {
	securityContext := container.SecurityContext
	if errSet := ValidateSecurityContext(securityContext); errSet != nil {
		errorSet = append(errorSet, errSet...)
	}
	if errSet := ValidateImage(container.Image); errSet != nil {
		errorSet = append(errorSet, errSet...)
	}
	return
}

func ValidateImage(image string) (errs ErrorSet) {
	if strings.Contains(image, "@sha256:") {
		return
	}
	if strings.HasSuffix(image, ":latest") {
		errs = append(errs, errors.New("image with latest tag"))
		return
	}
	if !strings.Contains(image, ":") {
		errs = append(errs, errors.New("image without tag (latest)"))
	}
	return
}

func ValidateSecurityContext(securityContext *v1.SecurityContext) (errs ErrorSet) {
	if securityContext == nil {
		errs = append(errs, errors.New("empty security context"))
		return
	}

	// https://kubesec.io/basics/containers-securitycontext-privileged-true/
	if securityContext.Privileged != nil {
		if *securityContext.Privileged {
			errs = append(errs, errors.New("privileged: true"))
		}
	}

	// https://kubesec.io/basics/containers-securitycontext-readonlyrootfilesystem-true/
	if securityContext.ReadOnlyRootFilesystem != nil {
		if !*securityContext.ReadOnlyRootFilesystem {
			errs = append(errs, errors.New("readOnlyRootFilesystem: false"))
		}
	} else {
		errs = append(errs, errors.New("readOnlyRootFilesystem: undefined"))
	}

	// https://kubesec.io/basics/containers-securitycontext-runasnonroot-true/
	if securityContext.RunAsNonRoot != nil {
		if !*securityContext.RunAsNonRoot {
			errs = append(errs, errors.New("runAsNonRoot: false"))
		}
	} else {
		errs = append(errs, errors.New("runAsNonRoot: undefined"))
	}

	// https://kubesec.io/basics/containers-securitycontext-runasuser/
	if securityContext.RunAsUser != nil {
		if *securityContext.RunAsUser > 10000 {
			errs = append(errs, errors.New("runAsUser: high-UID user"))
		}
	} else {
		errs = append(errs, errors.New("runAsUser: undefined"))
	}

	if securityContext.Capabilities != nil {
		caps := securityContext.Capabilities
		var dropAll, addSysAdmin bool
		for _, capability := range caps.Drop {
			if capability == "ALL" {
				dropAll = true
			}
		}
		for _, capability := range caps.Add {
			if capability == "SYS_ADMIN" {
				addSysAdmin = true
			}
		}
		// https://kubesec.io/basics/containers-securitycontext-capabilities-drop-index-all/
		if !dropAll {
			errs = append(errs, errors.New("capabilities.drop does not contain ALL"))
		}
		// https://kubesec.io/basics/containers-securitycontext-capabilities-add-index-sys-admin/
		if addSysAdmin {
			errs = append(errs, errors.New("capabilities.add contains SYS_ADMIN"))
		}
	} else {
		errs = append(errs, errors.New("capabilities: undefined"))
	}

	return
}
