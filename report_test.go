package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestReport_String(t *testing.T) {
	r := &Report{
		Items: []*ReportItem{
			{
				Namespace: "foobar",
			},
		},
	}
	assert.NotEmpty(t, r.String("yaml"))
	assert.NotEmpty(t, r.String("json"))
	assert.NotEmpty(t, r.String("csv"))
	assert.NotEmpty(t, r.String("foobar"))
}
